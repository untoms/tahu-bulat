package com.fasilkom.apap.uts1806269631.services;

import com.fasilkom.apap.uts1806269631.entities.Buku;

import java.util.List;

public interface BukuService {

    List<Buku> fetchAllBuku();
    Buku fetchBukuByKode(String kode);
    void addOrUpdate(Buku buku);
}
