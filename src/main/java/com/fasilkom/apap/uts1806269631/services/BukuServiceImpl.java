package com.fasilkom.apap.uts1806269631.services;

import com.fasilkom.apap.uts1806269631.entities.Buku;
import com.fasilkom.apap.uts1806269631.repositories.BukuRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BukuServiceImpl implements BukuService{

    private final BukuRepo bukuRepo;

    @Autowired
    public BukuServiceImpl(BukuRepo bukuRepo) {
        this.bukuRepo = bukuRepo;
    }

    @Override
    public List<Buku> fetchAllBuku() {
        return bukuRepo.findAll();
    }

    @Override
    public Buku fetchBukuByKode(String kode) {
        return bukuRepo.findByKode(kode).get();
    }

    @Override
    public void addOrUpdate(Buku buku) {
        bukuRepo.save(buku);
    }
}
