package com.fasilkom.apap.uts1806269631;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Uts1806269631Application {

	public static void main(String[] args) {
		SpringApplication.run(Uts1806269631Application.class, args);
	}

}
