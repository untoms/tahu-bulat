package com.fasilkom.apap.uts1806269631.repositories;

import com.fasilkom.apap.uts1806269631.entities.Buku;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface BukuRepo extends JpaRepository <Buku, Long> {

    Optional<Buku> findByKode(String kode);
}
