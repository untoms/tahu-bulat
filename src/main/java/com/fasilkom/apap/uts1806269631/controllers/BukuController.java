package com.fasilkom.apap.uts1806269631.controllers;

import com.fasilkom.apap.uts1806269631.entities.Buku;
import com.fasilkom.apap.uts1806269631.services.BukuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.util.List;

@Controller
public class BukuController {

    private final BukuService bukuService;

    @Autowired
    public BukuController(BukuService bukuService) {
        this.bukuService = bukuService;
    }

    @GetMapping("/")
    public String home(Model model){

        List<Buku> bukus = bukuService.fetchAllBuku();

        model.addAttribute("bukus", bukus);

        return "home";
    }

    @GetMapping("/buku/{kode}")
    public String getBuku(@PathVariable(name = "kode")String kode, Model model){

        model.addAttribute("buku", bukuService.fetchBukuByKode(kode));

        return "details_buku";
    }

    @GetMapping("/buku/update/{kode}")
    public String shoUpdate(@PathVariable(name = "kode")String kode, Model model){

        model.addAttribute("buku", bukuService.fetchBukuByKode(kode));

        return "form_buku";
    }

    @PostMapping("/buku/update")
    public String doUpdate(@ModelAttribute @Valid Buku buku, Model model){

        bukuService.addOrUpdate(buku);

        return "redirect:/buku/"+buku.getKode();
    }
}
