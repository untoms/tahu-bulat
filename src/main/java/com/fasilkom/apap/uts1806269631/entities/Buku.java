package com.fasilkom.apap.uts1806269631.entities;


import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "buku")
public class Buku {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "kode", length = 5, nullable = false, unique = true)
    private String kode;

    @Column(name = "judul", length = 150, nullable = false)
    private String judul;

    @Column(name = "jenis", nullable = false)
    @Enumerated(EnumType.ORDINAL)
    private Jenis jenis;

    @Column(name = "tanggal_terbit")
    @DateTimeFormat(pattern= "yyyy-mm-dd")
    private Date tanggalTerbit;

    @Column(name = "pengarang", length = 50)
    private String pengarang;

    @Column(name = "penerbit", length = 50)
    private String penerbit;

    @Column(name = "abstrak", length = 500)
    private String abstrak;

    @Column(name = "status")
    private int status;

    public enum Jenis {

        NA("-"),
        KARYA_TULIS("karya tulis"),
        MAJALAH_KORAN("majalah/koran"),
        CERITA_NOVEL("cerita/novel"),
        BUKU_AJAR("buku ajar"),
        LAINNYA("lainnya");

        private final String nama;

        private Jenis(String nama) {
            this.nama = nama;
        }

        @Override
        public String toString() {
            return nama;
        }
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public Jenis getJenis() {
        return jenis;
    }

    public void setJenis(Jenis jenis) {
        this.jenis = jenis;
    }

    public Date getTanggalTerbit() {
        return tanggalTerbit;
    }

    public void setTanggalTerbit(Date tanggalTerbit) {
        this.tanggalTerbit = tanggalTerbit;
    }

    public String getPengarang() {
        return pengarang;
    }

    public void setPengarang(String pengarang) {
        this.pengarang = pengarang;
    }

    public String getPenerbit() {
        return penerbit;
    }

    public void setPenerbit(String penerbit) {
        this.penerbit = penerbit;
    }

    public String getAbstrak() {
        return abstrak;
    }

    public void setAbstrak(String abstrak) {
        this.abstrak = abstrak;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
